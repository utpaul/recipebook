import {Component, ViewChild} from '@angular/core';
import {Platform, MenuController, NavController} from 'ionic-angular';
import { StatusBar, Splashscreen } from 'ionic-native';
import firebase from 'firebase';

import {TabsPage} from "../pages/tabs/tabs";
import {SigninPage} from "../pages/signin/signin";
import {SignupPage} from "../pages/signup/signup";
import {AuthService} from "../services/auth";


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage: any = TabsPage;
  sigininPage = SigninPage;
  signupPage = SignupPage;
  isAuthticated = false;

  @ViewChild('nav') nav: NavController;

  constructor(platform: Platform,
              private menuCtrl: MenuController,
              private authService: AuthService) {
    firebase.initializeApp({
      apiKey: "AIzaSyAn3QojttVVZK2QgGFrzAhelqZupiW8STo",
      authDomain: "ionic2-recipebook-a41f8.firebaseapp.com"
    });
    firebase.auth().onAuthStateChanged(user =>{
        if(user){
          this.isAuthticated =true;
          this.rootPage = TabsPage;
        }
        else{
          this.isAuthticated =false;
          this.rootPage = SigninPage;
        }
    });

    platform.ready().then(() => {
      StatusBar.styleDefault();
      Splashscreen.hide();
    });
  }

  onLoad(page: any){
    this.nav.setRoot(page);
    this.menuCtrl.close();
  }
  onLogout(){
    this.authService.logOut();
    this.menuCtrl.close();
    this.nav.setRoot(SigninPage);
  }
}
