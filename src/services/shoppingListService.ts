import {Ingredient} from "../models/ingredient";

import { Injectable } from "@angular/core";
import { Http, Response } from "@angular/http";
import 'rxjs/Rx';

import { AuthService } from "./auth";

@Injectable()

export class ShoppingListService{
  private ingredient: Ingredient[] =[];

  constructor(private http: Http,
              private authService: AuthService){}

  addItem(name: string, amount: number){
    this.ingredient.push(new Ingredient(name, amount));
  }

  addItems(items: Ingredient[]){
    this.ingredient.push(...items);
  }

  getItems(){
    return this.ingredient.slice();
  }

  removeItem(index: number){
    return this.ingredient.splice(index, 1);
  }

  storeList(token: string){
    const userid = this.authService.getActiveUser().uid;

    return this.http
      .put('https://ionic2-recipebook-a41f8.firebaseio.com/'+ userid +
      '/shopping-list.json?auth='+ token, this.ingredient)
      .map((response: Response) =>{
       return response.json();
      });
  }

  fetchList(token: string) {
    const userId = this.authService.getActiveUser().uid;
    return this.http.get('https://ionic2-recipebook-a41f8.firebaseio.com/' + userId + '/shopping-list.json?auth=' + token)
      .map((response: Response) => {
        return response.json();
      })
      .do((ingredients: Ingredient[]) => {
        if (ingredients) {
          this.ingredient = ingredients
        } else {
          this.ingredient = [];
        }
      });
  }

}
