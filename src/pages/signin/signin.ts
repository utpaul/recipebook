import { Component } from '@angular/core';
import {NgForm} from "@angular/forms";
import {AuthService} from "../../services/auth";
import {LoadingController, AlertController} from "ionic-angular";

@Component({
  selector: 'page-signin',
  templateUrl: 'signin.html'
})
export class SigninPage {

  constructor(private authService: AuthService,
              private loadingCtrl: LoadingController,
              private alertCtrl: AlertController) {}

  onSignIn(form: NgForm){
    const loader = this.loadingCtrl.create({
      content: "Signing you in...",
    });

    loader.present();

     this.authService.signIn(form.value.email, form.value.password)
       .then(data =>{
         loader.dismiss();
         form.reset();
       })
       .catch(error =>{

         form.reset();
         let alert = this.alertCtrl.create({
           title: 'SignIn Failed!',
           message: error.message,
           buttons: ['OK']
         });

         alert.present();

       })
  }

}
