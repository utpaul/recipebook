import {Component, OnInit} from '@angular/core';
import {NavParams, ActionSheetController, AlertController, ToastController, NavController} from 'ionic-angular';
import {FormGroup, FormControl, Validators, FormArray} from "@angular/forms";
import {RecipesService} from "../../services/recipes";
import {Recipe} from "../../models/recipe";

@Component({
  selector: 'page-edit-recipe',
  templateUrl: 'edit-recipe.html'
})
export class EditRecipePage implements OnInit{

  mode='new';
  selectOption=['Easy','Medium', 'hard'];
  recipeForm: FormGroup;
  recipe: Recipe;
  index: number;

  constructor(private navParams: NavParams,
              private actionSheetCtrl: ActionSheetController,
              private alertCtrl: AlertController,
              private toastCtrl: ToastController,
              private recipesService: RecipesService,
              private navCtrl: NavController ) {}

  ngOnInit(){

      this.mode = this.navParams.get('mode');

      if (this.mode == 'Edit') {
        this.recipe = this.navParams.get('recipe');
        this.index = this.navParams.get('index');
      }

    this.initializeForm();
  }

  onManageIngredients(){
      const actionSheet = this.actionSheetCtrl.create({

         title:'What do you want to do?',
         buttons:[
           {
             text:'Add Ingredient',
             handler: () => {
               this.createOnGreadientAlert().present();
             }
           },
           {
             text:'Remove All Ingredient',
             role:'destructive',
             handler: () => {
               const fArray:FormArray =<FormArray> this.recipeForm.get('ingredients');
               const len = fArray.length;
               for(let i= len-1; i>=0 ; i--){
                 fArray.removeAt(i);
               }
               const tostAleart = this.toastCtrl.create({
                 message: 'Remove all Gredients',
                 duration: 1000,
                 position: 'bottom'
               });
               tostAleart.present();
             }
           },
           {
             text:'Cancle',
             role: 'cancle'
           }
         ]
        })
    actionSheet.present();
  }
  private createOnGreadientAlert(){
    return this.alertCtrl.create({
      title:'Add Ingreadient',
      inputs: [
        {
          name: 'name',
          placeholder: 'Name'
        }
      ],
      buttons: [
        {
          text:'Cancel',
          role: 'cancle'
        },
        {
          text:'Add',
          handler: data => {
            if(data.name.trim()==''|| data.name == null){
              const tost = this.toastCtrl.create({
                message: 'Please enter valid value!',
                duration: 1000,
                position: 'bottom'
              });
              tost.present();
              return;
            }
            (<FormArray> this.recipeForm.get('ingredients')).
            push(new FormControl(data.name, Validators.required))
            const tost = this.toastCtrl.create({
              message: 'Item Added!',
              duration: 1000,
              position: 'bottom'
            });
            tost.present();
          }
        }
      ]
    })
  }

  onSubmit(){
   const value = this.recipeForm.value;
   let ingredients =[];
   if(value.ingredients.length>0){
     ingredients = value.ingredients.map(name=>{
       return {name: name, amount: 1};
     });
   }
    if (this.mode == 'Edit') {
      this.recipesService.updateRecipe(this.index, value.title, value.description, value.difficulty, ingredients);
    } else {
      this.recipesService.addRecipe(value.title, value.description, value.difficulty, ingredients);
    }
    this.recipeForm.reset();
    this.navCtrl.popToRoot();

  }

  private initializeForm() {
    let title = null;
    let description = null;
    let difficulty = 'Medium';
    let ingredients = [];

    if (this.mode == 'Edit') {
      title = this.recipe.title;
      description = this.recipe.description;
      difficulty = this.recipe.difficulty;
      for (let ingredient of this.recipe.ingredients) {
        ingredients.push(new FormControl(ingredient.name, Validators.required));
      }
    }

    this.recipeForm = new FormGroup({
      'title': new FormControl(title, Validators.required),
      'description': new FormControl(description, Validators.required),
      'difficulty': new FormControl(difficulty, Validators.required),
      'ingredients': new FormArray(ingredients)
    });
  }

}
