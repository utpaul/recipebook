import { Component } from '@angular/core';
import {NgForm} from "@angular/forms";
import {ShoppingListService} from "../../services/shoppingListService";
import {Ingredient} from "../../models/ingredient";
import {PopoverController, LoadingController, AlertController} from "ionic-angular";
import {AuthService} from "../../services/auth";
import {SLOptionPage} from "../../database-options/database-options";
@Component({
  selector: 'page-shopping-list',
  templateUrl: 'shopping-list.html'
})
export class ShoppingListPage {


 listItems :Ingredient[];

  constructor(private shoppingListService: ShoppingListService,
              private popoverCtrl: PopoverController,
              private authService: AuthService,
              private loadingCtrl: LoadingController,
              private alertCtrl: AlertController){

  }

  ionViewWillEnter(){
    this.loadItems();
  }

  onCheckItem(index: number){
    this.shoppingListService.removeItem(index);
    this.loadItems();
  }

  onAddItem(form: NgForm){
    this.shoppingListService.addItem(form.value.ingredientName, form.value.amount);
    form.reset();
    this.loadItems();
  }

  loadItems(){
    this.listItems = this.shoppingListService.getItems();
  }

  onShowOptions(event: MouseEvent){
    const loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });


    const popover = this.popoverCtrl.create(SLOptionPage);
    popover.present({ev: event});

    popover.onDidDismiss(data =>{

        if(data.action == 'load'){
          loading.present();
          this.authService.getActiveUser().getToken()
            .then(
              (token: string) => {
                this.shoppingListService.fetchList(token)
                  .subscribe(
                    (list: Ingredient[]) => {
                      loading.dismiss();
                      if (list) {
                        this.listItems = list;
                      } else {
                        this.listItems = [];
                      }
                    },
                    error => {
                      loading.dismiss();
                      this.handleError(error.json().error);
                    }
                  );
              }
            );

        }
        else{
          loading.present();
          this.authService.getActiveUser().getToken()
            .then(
              (token: string) =>{
                this.shoppingListService.storeList(token)
                  .subscribe(
                    () => loading.dismiss(),
                    error => {
                      loading.dismiss();
                      this.handleError(error.json().error);
                    }
                  )
              }
            )
        }
    });
  }

  private handleError(errorMessage: string) {
    const alert = this.alertCtrl.create({
      title: 'An error occurred!',
      message: errorMessage,
      buttons: ['Ok']
    });
    alert.present();
  }
}

