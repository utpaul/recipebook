import { Component } from '@angular/core';
import {NgForm} from "@angular/forms";
import {AuthService} from "../../services/auth";
import {LoadingController, AlertController} from "ionic-angular";

@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html'
})
export class SignupPage {

  constructor(private authService: AuthService,
              private loadingCtrl: LoadingController,
              private alertCtrl: AlertController) {}

  onSignUp(form:NgForm){

    const loader = this.loadingCtrl.create({
      content: "Sign in...",
    });
    loader.present();

    this.authService.signUp(form.value.email, form.value.password)
      .then(data =>{
        loader.dismiss();
        form.reset();
      })
      .catch(error =>{
        form.reset();
        let alert = this.alertCtrl.create({
          title: 'SignUp Failed!',
          message: error.message,
          buttons: ['OK']
        });

        alert.present();
      });

  }

}
